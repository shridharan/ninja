Assignments


Must Do

Initialize a local git repository 


  A- git init

 
Run git status command
 
Create a file README.md 

   A- touch README.md 


Run git status command 
    
A- git status will show there is oneuntracked file README.md



Add file README.md in your local repository

     A- git add README.md 
  


Add My first interaction with Git content in README.md
       
   A-vim README.md
            “add content” 



Run git status command 
Update file README.md in your local repository

    A-git add README.md 



 Run git status command 
    
    A- git status will show that there is one file to be commited


Create files README1.md README2.md README3.md README4.md 
      
    A-touch README1.md README2.md README3.md README.md4


Run git status command 
 
  A- status command will show there are 4 untracked files 



Add all the newly added files in your local repository without giving their name. 
   
 A-Git add .  -  This will add all the untracked files in the statging area .



Run git status command 
 
    A-now the status command will show 4 files are there to be commited



Remove files README1.md README2.md from local repository 
      
       A-rm  README1.md README2.md



Add content in Temporary content in README3.md 
 add some content in README3.md 



Run git status command 
    
    A- now git status will show , two file README1.md README2.md are deleted
      and one file README3.md is modified.



Find out the content that is added in README3.md 
Undo the changes in README3.md file 



Run git status command 
Create a branch ninja from current branch 

   A-git branch ninja



Create sensei branch from ninja branch and switch to sensei branch
  
A- git branch sensei  
      git checkout sensei



List out all the local branches 

     A- git branch


Delete sensei branch 
  
  A- git branch -d sensei
  


Find out the current branch 
  
     A-git branch -a


List out all the commits that have been done 
     
    A- git log




Assignments 2

Must Do

Create a GitLab account if it doesn't exists already. 

Create a repo GitCommand in your GitLab account . 

Delete repo *GitCommand" 

Fork https://github.com/joshnh/Git-Commands in your GitLab account with the name 
GitCommand 

Clone GitCommand repo using http protocol 
     
        A- git clone “url”


Delete the cloned code 
   
      A- rm -rm git-command

Clone GitCommand repo using git protocol at ~/ninja 
  
    A- cd ninja 
   git clone “url”


Create a folder ninja 
    
     A- mkdir ninja
    
Create a file README.md in ninja folder
    A- touch README.md 


Make your changes available to GitCommand remote repo
     A- git add .
          Git commit -m “ commit message
           git push origin master 


Good To Do


Get the latest code from remote repo without using git pull command 
Simulate above problem where instead of GitLab you would be using Git file server 


Simulate above problem where instead of GitLab you would be using custom Git SSH server 
